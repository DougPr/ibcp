/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ViewMenu;

import Classes.GerarRelatorio;
import Classes.Pendencias;
import Classes.PendenciasDAO;
import ViewRelatorio.ConsultaPorTurmaTela;
import ViewRelatorio.VecimentoMes;
import ViewRelatorio.VencimentoRelPendente;
import ViewRelatorio.clienteRel;
import ViewRelatorio.pagamentoRel;
import ViewRelatorio.psicanalistaRel;
import com.mysql.cj.result.Row;
import java.awt.Color;
import java.awt.Component;
import static java.awt.Frame.MAXIMIZED_BOTH;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.List;
import java.awt.Point;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

/**
 *
 * @author dougl
 */
public class telaPendencia extends javax.swing.JFrame {

    Pendencias objPendencia;

    /**
     * Creates new form telaPendencia
     */
    public telaPendencia() {
        initComponents();
        setExtendedState(MAXIMIZED_BOTH);
        setTitle("Pendências");
        ImageIcon icone = new ImageIcon(getClass().getResource("/resources/System-computer-icon.png"));
        this.setIconImage(icone.getImage());
        CarregarJTable();
        objPendencia = new Pendencias();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton5 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        lblTotalPendencias = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenuItem10 = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenuItem11 = new javax.swing.JMenuItem();
        jMenuItem12 = new javax.swing.JMenuItem();
        jMenuItem13 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        jMenuItem14 = new javax.swing.JMenuItem();

        jButton5.setText("jButton5");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Titulo", "Status", "Data Criação", "Última Atualização", "Descrição", "Execução"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        lblTotalPendencias.setText("jLabel1");

        jButton1.setText("Excluir");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Exportar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Abrir");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("Novo");

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Menu-icon.png"))); // NOI18N
        jMenu1.setText("Menu");

        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/home-icon.png"))); // NOI18N
        jMenuItem2.setText("Tela Principal");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuItem9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Clients-icon.png"))); // NOI18N
        jMenuItem9.setText("Cadastrar Cliente ");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem9);

        jMenuItem10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Office-Customer-Male-Light-icon.png"))); // NOI18N
        jMenuItem10.setText("Cadastrar Psicanalista");
        jMenuItem10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem10ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem10);

        jMenu4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/product-icon.png"))); // NOI18N
        jMenu4.setText("Turma ");

        jMenuItem11.setText("Cadastrar Turma");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem11);

        jMenuItem12.setText("Matricula Aluno");
        jMenuItem12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem12ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem12);

        jMenu1.add(jMenu4);

        jMenuItem13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/cliente-icon.png"))); // NOI18N
        jMenuItem13.setText("Usuário");
        jMenuItem13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem13ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem13);
        jMenu1.add(jSeparator1);

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Log-Out-icon.png"))); // NOI18N
        jMenuItem1.setText("Sair");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        jMenu2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/chart-icon.png"))); // NOI18N
        jMenu2.setText("Relátorio");

        jMenuItem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Groups-Meeting-Dark-icon.png"))); // NOI18N
        jMenuItem3.setText("Cliente");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem3);

        jMenuItem4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Office-Customer-Male-Light-icon.png"))); // NOI18N
        jMenuItem4.setText("Psicanalistas");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem4);

        jMenuItem5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/product-icon.png"))); // NOI18N
        jMenuItem5.setText("Turma");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem5);

        jMenu3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Money-icon.png"))); // NOI18N
        jMenu3.setText("Pagamentos");

        jMenuItem6.setText("Listagem de Pagamento");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem6);

        jMenuItem7.setText("Mensalidades Vencidas");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem7);

        jMenuItem8.setText("Mensalidades Vencidas no Mês");
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem8ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem8);

        jMenu2.add(jMenu3);

        jMenu5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Finance-Bill-icon.png"))); // NOI18N
        jMenu5.setText("Book");

        jMenuItem14.setText("Cliente");
        jMenuItem14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem14ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem14);

        jMenu2.add(jMenu5);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblTotalPendencias)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 451, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(8, 8, 8))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton3)
                        .addGap(18, 18, 18)
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton1))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblTotalPendencias)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 222, Short.MAX_VALUE)))
                .addGap(29, 29, 29))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        telaPrincipal tp = new telaPrincipal();
        tp.setVisible(true);
        try {
            Thread.sleep(300);
        } catch (InterruptedException ex) {
            Logger.getLogger(telaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dispose();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {
            if (jTable1.getRowCount() > 0) {
                //Resgato o número da linha pelo JTable
                int numeroLinha = jTable1.getSelectedRow();

                //Resgato o ID (oculto) do cliente pelo JTableModel
                int ID = Integer.parseInt(jTable1.getModel().getValueAt(numeroLinha, 0).toString());

                objPendencia.setId(ID);

                if (PendenciasDAO.excluir(objPendencia.getId())) {
                    JOptionPane.showMessageDialog(this, "Cliente excluído com sucesso!");
                } else {
                    JOptionPane.showMessageDialog(this, "Falha o excluir cliente!");
                }

                //Consulto novamente a base de dados
                CarregarJTable();

            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Selecione um cliente da tabela!" + e.getMessage(),
                    "Aviso de Falha", JOptionPane.ERROR_MESSAGE);
        }
        CarregarJTable();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try {
            Date hoje = new Date();
            SimpleDateFormat sdf1 = new SimpleDateFormat("MMMMM yyyy"); //você pode usar outras máscaras
            String t = sdf1.format(hoje);

            JFileChooser fc = new JFileChooser();

            JFrame framePrincipal = new JFrame();
            framePrincipal.setLocationRelativeTo(null);
            Image image = ImageIO.read(getClass().getResource("/resources/System-computer-icon.png"));
            framePrincipal.setIconImage(image);

            fc.setSelectedFile(new File("Relátorio IBCP Pendencias " + t + ".xls"));
            fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
            int i = fc.showSaveDialog(framePrincipal);
            //fc.showOpenDialog(this);
            if (i == 1) {
            } else {
                try {
                    File arquivo = fc.getSelectedFile();

                    WritableFont fontbol = new WritableFont(WritableFont.ARIAL, 10);
                    fontbol.setBoldStyle(WritableFont.BOLD);
                    WritableCellFormat cellFormat = new WritableCellFormat(fontbol);
                    jxl.format.Colour bckcolor = jxl.format.Colour.AQUA;
                    cellFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
                    cellFormat.setVerticalAlignment(jxl.format.VerticalAlignment.JUSTIFY);
                    WritableCellFormat cellFormatt = new WritableCellFormat();
                    cellFormatt.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);

                    try {
                        cellFormat.setBackground(bckcolor);
                    } catch (WriteException ex) {
                        Logger.getLogger(psicanalistaRel.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    try {
                        //Instaciando a classe que gera o novo arquivo do excel
                        WritableWorkbook workbook = Workbook.createWorkbook(arquivo);

                        //criando uma nova planilha
                        WritableSheet sheet = workbook.createSheet("Pendencias", 0);

                        //col, lin
                        Label labelTitulo = new Label(0, 0, "Titulo");
                        sheet.addCell(labelTitulo);
                        sheet.setColumnView(0, 39);
                        labelTitulo.setCellFormat(cellFormat);

                        Label labelTitulo2 = new Label(1, 0, "Cumprida");
                        sheet.addCell(labelTitulo2);
                        sheet.setColumnView(1, 10);
                        labelTitulo2.setCellFormat(cellFormat);

                        Label labelTitulo3 = new Label(2, 0, "Data Criação");
                        sheet.addCell(labelTitulo3);
                        sheet.setColumnView(2, 19);
                        labelTitulo3.setCellFormat(cellFormat);

                        Label labelTitulo4 = new Label(3, 0, "Última Atualização");
                        sheet.addCell(labelTitulo4);
                        sheet.setColumnView(3, 18);
                        labelTitulo4.setCellFormat(cellFormat);

                        Label labelTitulo5 = new Label(4, 0, "Descrição");
                        sheet.addCell(labelTitulo5);
                        sheet.setColumnView(4, 38);
                        labelTitulo5.setCellFormat(cellFormat);

                        Label labelTitulo6 = new Label(5, 0, "Execução");
                        sheet.addCell(labelTitulo6);
                        sheet.setColumnView(5, 32);
                        labelTitulo6.setCellFormat(cellFormat);

                        int contador = 1;
                        PendenciasDAO pdao = new PendenciasDAO();
                        for (Pendencias p : pdao.todasPendencias()) {

                            Label label1 = new Label(0, contador, p.getTitulo());
                            sheet.addCell(label1);
                            label1.setCellFormat(cellFormatt);
                            Label label2 = new Label(1, contador, p.getCumprida());
                            sheet.addCell(label2);
                            label2.setCellFormat(cellFormatt);
                            Label label3 = new Label(2, contador, p.getCriacao().toString());
                            sheet.addCell(label3);
                            label3.setCellFormat(cellFormatt);
                            Label label4 = new Label(3, contador, p.getLastUpdate().toString());
                            sheet.addCell(label4);
                            label4.setCellFormat(cellFormatt);
                            Label label5 = new Label(4, contador, p.getDescrição());
                            sheet.addCell(label5);
                            label5.setCellFormat(cellFormatt);
                            Label label6 = new Label(5, contador, p.getExecucao());
                            sheet.addCell(label6);
                            label6.setCellFormat(cellFormatt);

                            contador++;
                        }

                        //escrevendo o arquivo em disco
                        workbook.write();

                        //Fechando a IO
                        workbook.close();
                        JOptionPane.showMessageDialog(this, "Excel Gerado com sucesso!");
                    } catch (IOException ex) {
                        Logger.getLogger(clienteRel.class.getName()).log(Level.SEVERE, null, ex);
                        JOptionPane.showMessageDialog(this, "Excel Aberto!");
                    } catch (WriteException ex) {
                        Logger.getLogger(clienteRel.class.getName()).log(Level.SEVERE, null, ex);
                        JOptionPane.showMessageDialog(this, "Excel Aberto!");
                    }

                } catch (WriteException ex) {
                    Logger.getLogger(telaPendencia.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        } catch (IOException ex) {
            Logger.getLogger(telaPendencia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        try {
            DefaultTableModel dados = (DefaultTableModel) jTable1.getModel();
            dados.setNumRows(0);
            JFileChooser fc = new JFileChooser();

            fc.setFileSelectionMode(JFileChooser.FILES_ONLY);

            JFrame framePrincipal = new JFrame();
            framePrincipal.setLocationRelativeTo(null);
            Image image = ImageIO.read(getClass().getResource("/resources/System-computer-icon.png"));
            framePrincipal.setIconImage(image);

            //ImageIcon icone = new ImageIcon(getClass().getResource("/resources/System-computer-icon.png"));
            int i = fc.showOpenDialog(framePrincipal);

            //fc.showOpenDialog(this);
            if (i == 1) {
            } else {
                File arquivo = fc.getSelectedFile();
                String d = arquivo.toString();

                try {
                    Workbook workbook = Workbook.getWorkbook(arquivo);

                    Sheet sheet = workbook.getSheet(0);

                    //Cell c0 = sheet.getCell(0, 0);
                    //String Titulo = c0.getContents();
                    int linhas = sheet.getRows();

                    for (int z = 1; z < linhas; z++) {

                        Cell ca = sheet.getCell(0, z);
                        Cell cb = sheet.getCell(1, z);
                        Cell cc = sheet.getCell(2, z);
                        Cell cd = sheet.getCell(3, z);
                        Cell ce = sheet.getCell(4, z);
                        Cell cf = sheet.getCell(5, z);

                        String titulo = ca.getContents();
                        String Status = cb.getContents();
                        String Datacriacao = cc.getContents();
                        String ultima = cd.getContents();
                        String desc = ce.getContents();
                        String exec = cf.getContents();

                        // DefaultTableModel modelo = (DefaultTableModel) jTable1.getModel();
                        dados.addRow(new String[]{null, titulo, Status, Datacriacao, ultima, desc, exec});

                    }
                    workbook.close();
                } catch (IOException | BiffException ex) {
                    Logger.getLogger(telaPendencia.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        } catch (IOException ex) {
            Logger.getLogger(telaPendencia.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_jButton3ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        clienteRel cr = new clienteRel();
        cr.setVisible(true);
        try {
            Thread.sleep(300);
        } catch (InterruptedException ex) {
            Logger.getLogger(telaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dispose();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        psicanalistaRel pr = new psicanalistaRel();
        pr.setVisible(true);
        try {
            Thread.sleep(300);
        } catch (InterruptedException ex) {
            Logger.getLogger(telaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dispose();
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        ConsultaPorTurmaTela cptt = new ConsultaPorTurmaTela();
        cptt.setVisible(true);
        try {
            Thread.sleep(300);
        } catch (InterruptedException ex) {
            Logger.getLogger(telaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dispose();
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed
        TelaCadCliente cad = new TelaCadCliente();
        cad.setVisible(true);
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    private void jMenuItem10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem10ActionPerformed
        telaCadastroPsicanalista cadp = new telaCadastroPsicanalista();
        cadp.setVisible(true);
    }//GEN-LAST:event_jMenuItem10ActionPerformed

    private void jMenuItem12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem12ActionPerformed
        clienteRel cr = new clienteRel();
        cr.setVisible(true);
        try {
            Thread.sleep(300);
        } catch (InterruptedException ex) {
            Logger.getLogger(telaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dispose();
    }//GEN-LAST:event_jMenuItem12ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        TelaCadTurma ct = new TelaCadTurma();
        ct.setVisible(true);
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void jMenuItem13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem13ActionPerformed
        telaUsuario tu = new telaUsuario();
        tu.setVisible(true);
    }//GEN-LAST:event_jMenuItem13ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        pagamentoRel pr = new pagamentoRel();
        pr.setVisible(true);
        try {
            Thread.sleep(300);
        } catch (InterruptedException ex) {
            Logger.getLogger(telaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dispose();
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem8ActionPerformed
        VecimentoMes vencm = new VecimentoMes();
        vencm.setVisible(true);
        try {
            Thread.sleep(300);
        } catch (InterruptedException ex) {
            Logger.getLogger(telaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dispose();
    }//GEN-LAST:event_jMenuItem8ActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        VencimentoRelPendente vr = new VencimentoRelPendente();
        vr.setVisible(true);
        try {
            Thread.sleep(300);
        } catch (InterruptedException ex) {
            Logger.getLogger(telaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dispose();
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem14ActionPerformed
        GerarRelatorio rel = new GerarRelatorio();
        try{
           rel.gerar();
        }catch (Exception e){
            //JOptionPane.showMessageDialog(null, "erro ao gerar relatorio"+e);
            System.out.println(e);
        }
    }//GEN-LAST:event_jMenuItem14ActionPerformed

    DefaultTableCellRenderer renderer = new DefaultTableCellRenderer() {
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

            String str = (String) value;
            if ("Não".equals(str)) {
                c.setForeground(Color.RED);
            } else {
                c.setForeground(Color.BLACK);
            }
            return c;
        }
    };

    private void CarregarJTable() {
        DefaultTableModel modelo = (DefaultTableModel) jTable1.getModel();

        PendenciasDAO pdao = new PendenciasDAO();
        modelo.setRowCount(0);
        int contador = 0;
        int ativos = 0;
        for (Pendencias p : pdao.todasPendencias()) {
            modelo.addRow(new Object[]{p.getId(), p.getTitulo(), p.getCumprida(), p.getCriacao(), p.getLastUpdate(),
                p.getDescrição(), p.getExecucao()});
            contador++;
            if (p.getCumprida().equals("Sim")) {
                ativos++;
            }
            jTable1.getColumnModel().getColumn(2).setCellRenderer(renderer);
        }

        String t = Integer.toString(contador);
        String a = Integer.toString(ativos);

        if (ativos > 0) {
            lblTotalPendencias.setForeground(Color.RED);
        }
        //lblTotal.setText(t);
        lblTotalPendencias.setText("Total de Pendências Ativas " + a);
        //para ocultar a coluna ID
        jTable1.getColumnModel().getColumn(0).setMinWidth(0);
        jTable1.getColumnModel().getColumn(0).setMaxWidth(0);

        jTable1.getColumnModel().getColumn(1).setMinWidth(150);
        jTable1.getColumnModel().getColumn(1).setMaxWidth(150);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(telaPendencia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(telaPendencia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(telaPendencia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(telaPendencia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new telaPendencia().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem12;
    private javax.swing.JMenuItem jMenuItem13;
    private javax.swing.JMenuItem jMenuItem14;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel lblTotalPendencias;
    // End of variables declaration//GEN-END:variables

}
