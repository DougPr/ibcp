       
package Classes;

import java.util.Date;

/**
 *
 * @author dougl
 */
public class psicanalista {
    private int id;
    private String ativo;
    private String didata;
    private String nome;
    private String observacao;
    private Date datamatri;

     public psicanalista() {
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Date getDatamatri() {
        return datamatri;
    }

    public void setDatamatri(Date datamatri) {
        this.datamatri = datamatri;
    }
     
     
   
    public psicanalista (int pid,String pativo, String pdidata,String pnome, String pobservacao){
        this.id = pid;
        this.ativo = pativo;
        this.didata = pdidata;
        this.nome = pnome;
        this.observacao = pobservacao;
    }

    public String getAtivo() {
        return ativo;
    }

    public void setAtivo(String ativo) {
        this.ativo = ativo;
    }
    
    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDidata() {
        return didata;
    }

    public void setDidata(String didata) {
        this.didata = didata;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getobservacao() {
        return observacao;
    }

    public void setobservacao(String observacao) {
        this.observacao = observacao;
    }
    
    
}
