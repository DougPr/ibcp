/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author dougl
 */
public class MatriculaDAO {
     public static boolean salvar(matricula p) {
        boolean retorno = false;
        Connection conexao = null;
        PreparedStatement instrucaoSQL = null;

        try {

            //Passo 1
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Passo 2 - DriverManager para abrir a conexão
            String URL = "jdbc:mysql://localhost:3306/ibcp?useTimezone=true&serverTimezone=UTC&useSSL=false";

            conexao = DriverManager.getConnection(URL, "root", "1234");
            conexao = GerenciadorConexao.abrirConexao();

            //Passo 3 - Executar uma instrução SQL
            instrucaoSQL = conexao.prepareStatement("call fazerMatricula (?,?,?,?,?,?)");

            //Adiciono os parâmetros ao meu comando SQL
            instrucaoSQL.setInt(1, p.getId());
            instrucaoSQL.setString(2, p.getAtivo());
            instrucaoSQL.setInt(3, p.getDiavenc());
            instrucaoSQL.setDate(4, new java.sql.Date(p.getDatamatri().getTime()));
            instrucaoSQL.setString(5, p.getObservacao());
            String asString = Character.toString(p.getTurma());
            instrucaoSQL.setString(6, asString);
         

            //Executar a instrução SQL
            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                retorno = true;

            }

        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
            retorno = false;
        } finally {

            //Libero os recursos da memória
            try {
                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                }

                GerenciadorConexao.fecharConexao();
                conexao.close();

            } catch (SQLException ex) {
            }
        }

        return retorno;
    }

}