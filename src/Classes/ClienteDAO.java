package Classes;

import Classes.Cliente;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import static jdk.nashorn.internal.objects.Global.getDate;

public class ClienteDAO {

    public static boolean salvar(Cliente p) {
        boolean retorno = false;
        Connection conexao = null;
        PreparedStatement instrucaoSQL = null;

        try {

            //Passo 1
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Passo 2 - DriverManager para abrir a conexão
            String URL = "jdbc:mysql://localhost:3306/ibcp?useTimezone=true&serverTimezone=UTC&useSSL=false";

            conexao = DriverManager.getConnection(URL, "root", "1234");
            conexao = GerenciadorConexao.abrirConexao();

            //Passo 3 - Executar uma instrução SQL
            instrucaoSQL = conexao.prepareStatement("INSERT INTO aluno VALUES(null,1,?,?,?,?,?,?,?,?,?,?)");

            //Adiciono os parâmetros ao meu comando SQL
            instrucaoSQL.setString(1, p.getNome());
            instrucaoSQL.setString(2, p.getSexo());
            instrucaoSQL.setString(3, p.getCpf());
            instrucaoSQL.setString(4, p.getEmail());
            instrucaoSQL.setString(5, p.getEndereco());
            instrucaoSQL.setString(6, p.getRg());
            instrucaoSQL.setString(7, p.getTel_celular());
            instrucaoSQL.setString(8, p.getTel_comercial());
            instrucaoSQL.setString(9, p.getTel_residencial());
            instrucaoSQL.setString(10, p.getObservacao());

            //Executar a instrução SQL
            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                retorno = true;

            }

        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
            retorno = false;
        } finally {

            //Libero os recursos da memória
            try {
                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                }

                GerenciadorConexao.fecharConexao();
                conexao.close();

            } catch (SQLException ex) {
            }
        }

        return retorno;
    }

    public static boolean excluir(String pNome) {
        boolean retorno = false;
        Connection conexao = null;
        PreparedStatement instrucaoSQL = null;

        try {

            //Tenta estabeler a conexão com o SGBD e cria comando a ser executado conexão
            //Obs: A classe GerenciadorConexao já carrega o Driver e define os parâmetros de conexão
            //Passo 1
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Passo 2 - DriverManager para abrir a conexão
            String URL = "jdbc:mysql://localhost:3306/ibcp?useTimezone=true&serverTimezone=UTC&useSSL=false";

            conexao = DriverManager.getConnection(URL, "root", "1234");

            instrucaoSQL = conexao.prepareStatement("DELETE FROM aluno WHERE nome = ?");

            //Adiciono os parâmetros ao meu comando SQL
            instrucaoSQL.setString(1, pNome);

            //Mando executar a instrução SQL
            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                retorno = true;
            } else {
                retorno = false;
            }

        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
            retorno = false;
        } finally {

            //Libero os recursos da memória
            try {
                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                }

                GerenciadorConexao.fecharConexao();
                conexao.close();

            } catch (SQLException ex) {
            }
        }

        return retorno;
    }

    public static ArrayList<Cliente> todosClientes() {
        ResultSet rs = null;
        Connection conexao = null;
        PreparedStatement instrucaoSQL = null;

        //Armazeno as informaçoes da tabela (resultSet) em um ArrayList
        ArrayList<Cliente> listaClientes = new ArrayList<Cliente>();

        try {

            conexao = GerenciadorConexao.abrirConexao();
            //Passo 1
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Passo 2 - DriverManager para abrir a conexão
            String URL = "jdbc:mysql://localhost:3306/ibcp?useTimezone=true&serverTimezone=UTC&useSSL=false";

            conexao = DriverManager.getConnection(URL, "root", "1234");

            //Passo 3 - Executo a instrução SQL
            instrucaoSQL = conexao.prepareStatement("call consulta");

            //Executa a Query (Consulta) - Retorna um objeto da classe ResultSet
            rs = instrucaoSQL.executeQuery();

            //Percorrer o resultSet
            while (rs.next()) {
                Cliente p = new Cliente();
                p.setId(rs.getInt("ID"));
                p.setNome(rs.getString("nome"));
                p.setSexo(rs.getString("sexo"));
                p.setCpf(rs.getString("cpf"));
                p.setEmail(rs.getString("email"));
                p.setEndereco(rs.getString("endereco"));
                p.setRg(rs.getString("rg"));
                p.setTel_celular(rs.getString("tel_celular"));
                p.setTel_comercial(rs.getString("tel_comercial"));
                p.setTel_residencial(rs.getString("tel_residencial"));
                p.setObservacao(rs.getString("observacao"));
                p.setAtivo(rs.getString("Ativo"));

                //Adiciono na listaClientes
                listaClientes.add(p);
            }

        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
            listaClientes = null;
        } finally {
            //Libero os recursos da memória
            try {
                if (rs != null) {
                    rs.close();
                }
                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                }

                conexao.close();
                GerenciadorConexao.fecharConexao();

            } catch (SQLException ex) {
            }
        }

        return listaClientes;
    }

    public static ArrayList<Cliente> consultarClientes(String pNome) {
        ResultSet rs = null;
        Connection conexao = null;
        PreparedStatement instrucaoSQL = null;

        ArrayList<Cliente> listaClientes = new ArrayList<Cliente>();

        try {

            conexao = GerenciadorConexao.abrirConexao();
            instrucaoSQL = conexao.prepareStatement("call consultaNomecli(?)");

            //Adiciono os parâmetros ao meu comando SQL
            instrucaoSQL.setString(1, "%" + pNome + '%');

            rs = instrucaoSQL.executeQuery();

            while (rs.next()) {
                Cliente c = new Cliente();
                c.setId(rs.getInt("ID"));
                c.setNome(rs.getString("nome"));
                c.setSexo(rs.getString("sexo"));
                c.setCpf(rs.getString("cpf"));
                c.setEmail(rs.getString("email"));
                c.setEndereco(rs.getString("endereco"));
                c.setRg(rs.getString("rg"));
                c.setTel_celular(rs.getString("tel_celular"));
                c.setTel_comercial(rs.getString("tel_comercial"));
                c.setTel_residencial(rs.getString("tel_residencial"));
                c.setObservacao(rs.getString("observacao"));
                c.setAtivo(rs.getString("Ativo"));

                listaClientes.add(c);
            }

        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
            listaClientes = null;
        } finally {
            //Libero os recursos da memória
            try {
                if (rs != null) {
                    rs.close();
                }
                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                }

                GerenciadorConexao.fecharConexao();

            } catch (SQLException ex) {
            }
        }

        return listaClientes;
    }

    public static boolean alterar(Cliente p) {
        boolean retorno = false;
        Connection conexao = null;
        PreparedStatement instrucaoSQL = null;

        try {

            //Tenta estabeler a conexão com o SGBD e cria comando a ser executado conexão
            //Obs: A classe GerenciadorConexao já carrega o Driver e define os parâmetros de conexão
            conexao = GerenciadorConexao.abrirConexao();

            //Passo 1
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Passo 2 - DriverManager para abrir a conexão
            String URL = "jdbc:mysql://localhost:3306/ibcp?useTimezone=true&serverTimezone=UTC&useSSL=false";

            conexao = DriverManager.getConnection(URL, "root", "1234");

            instrucaoSQL = conexao.prepareStatement("UPDATE aluno SET nome=?,sexo=?,"
                    + "cpf=?,email=?,endereco=?,rg=?,tel_celular=?,tel_comercial=?,"
                    + "tel_residencial=?,observacao=? WHERE id = ?");

            //Adiciono os parâmetros ao meu comando SQL
            instrucaoSQL.setString(1, p.getNome());
            instrucaoSQL.setString(2, p.getSexo());
            instrucaoSQL.setString(3, p.getCpf());
            instrucaoSQL.setString(4, p.getEmail());
            instrucaoSQL.setString(5, p.getEndereco());
            instrucaoSQL.setString(6, p.getRg());
            instrucaoSQL.setString(7, p.getTel_celular());
            instrucaoSQL.setString(8, p.getTel_comercial());
            instrucaoSQL.setString(9, p.getTel_residencial());
            instrucaoSQL.setString(10, p.getObservacao());
            instrucaoSQL.setInt(11, p.getId());

            //Mando executar a instrução SQL
            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                retorno = true;
            } else {
                retorno = false;
            }

        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
            retorno = false;
        } finally {

            //Libero os recursos da memória
            try {
                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                }

                GerenciadorConexao.fecharConexao();
                conexao.close();

            } catch (SQLException ex) {
            }
        }

        return retorno;
    }

    public static ArrayList<login> login(String pusuario) {
        ResultSet rs = null;
        Connection conexao = null;
        PreparedStatement instrucaoSQL = null;

        ArrayList<login> listaClientes = new ArrayList<login>();

        try {

            conexao = GerenciadorConexao.abrirConexao();
            instrucaoSQL = conexao.prepareStatement("call login (?);");

            //Adiciono os parâmetros ao meu comando SQL
            instrucaoSQL.setString(1, "%" + pusuario + '%');

            rs = instrucaoSQL.executeQuery();

            while (rs.next()) {
                login c = new login();

                c.setUsuario(rs.getString("username"));
                c.setPassword(rs.getString("password"));

                listaClientes.add(c);
            }

        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
            listaClientes = null;
        } finally {
            //Libero os recursos da memória
            try {
                if (rs != null) {
                    rs.close();
                }
                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                }

                GerenciadorConexao.fecharConexao();

            } catch (SQLException ex) {
            }
        }

        return listaClientes;
    }

    public static ArrayList<login> usuario() {
        ResultSet rs = null;
        Connection conexao = null;
        PreparedStatement instrucaoSQL = null;

        ArrayList<login> listaClientes = new ArrayList<login>();

        try {

            conexao = GerenciadorConexao.abrirConexao();
            instrucaoSQL = conexao.prepareStatement("call Usuario;");

            //Adiciono os parâmetros ao meu comando SQL
            // instrucaoSQL.setString(1, "%" + pusuario + '%');
            rs = instrucaoSQL.executeQuery();

            while (rs.next()) {
                login c = new login();

                c.setUsuario(rs.getString("usuario"));
                c.setPassword(rs.getString("senha"));

                listaClientes.add(c);
            }

        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
            listaClientes = null;
        } finally {
            //Libero os recursos da memória
            try {
                if (rs != null) {
                    rs.close();
                }
                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                }

                GerenciadorConexao.fecharConexao();

            } catch (SQLException ex) {
            }
        }
        return listaClientes;
    }

    public static boolean alterarpass(login p) {
        boolean retorno = false;
        Connection conexao = null;
        PreparedStatement instrucaoSQL = null;

        try {

            //Tenta estabeler a conexão com o SGBD e cria comando a ser executado conexão
            //Obs: A classe GerenciadorConexao já carrega o Driver e define os parâmetros de conexão
            conexao = GerenciadorConexao.abrirConexao();

            //Passo 1
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Passo 2 - DriverManager para abrir a conexão
            String URL = "jdbc:mysql://localhost:3306/ibcp?useTimezone=true&serverTimezone=UTC&useSSL=false";

            conexao = DriverManager.getConnection(URL, "root", "1234");

            instrucaoSQL = conexao.prepareStatement("UPDATE usuario SET password = ? WHERE username =? ");

            //Adiciono os parâmetros ao meu comando SQL
            instrucaoSQL.setString(1, p.getPassword());
            instrucaoSQL.setString(2, p.getUsuario());

            //Mando executar a instrução SQL
            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                retorno = true;
            } else {
                retorno = false;
            }

        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
            retorno = false;
        } finally {

            //Libero os recursos da memória
            try {
                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                }

                GerenciadorConexao.fecharConexao();
                conexao.close();

            } catch (SQLException ex) {
            }
        }

        return retorno;
    }

    public static ArrayList<psicanalista> todosPsicanalistas() {
        ResultSet rs = null;
        Connection conexao = null;
        PreparedStatement instrucaoSQL = null;

        //Armazeno as informaçoes da tabela (resultSet) em um ArrayList
        ArrayList<psicanalista> listaClientes = new ArrayList<psicanalista>();

        try {

            conexao = GerenciadorConexao.abrirConexao();
            //Passo 1
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Passo 2 - DriverManager para abrir a conexão
            String URL = "jdbc:mysql://localhost:3306/ibcp?useTimezone=true&serverTimezone=UTC&useSSL=false";

            conexao = DriverManager.getConnection(URL, "root", "1234");

            //Passo 3 - Executo a instrução SQL
            instrucaoSQL = conexao.prepareStatement("call PsicanalistasConsulta();");

            //Executa a Query (Consulta) - Retorna um objeto da classe ResultSet
            rs = instrucaoSQL.executeQuery();

            //Percorrer o resultSet
            while (rs.next()) {
                psicanalista p = new psicanalista();
                p.setId(rs.getInt("ID"));
                p.setAtivo(rs.getString("Ativo"));
                p.setDidata(rs.getString("didata"));
                p.setNome(rs.getString("nome"));
                p.setobservacao(rs.getString("observacao"));

                //Adiciono na listaClientes
                listaClientes.add(p);
            }

        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
            listaClientes = null;
        } finally {
            //Libero os recursos da memória
            try {
                if (rs != null) {
                    rs.close();
                }
                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                }

                conexao.close();
                GerenciadorConexao.fecharConexao();

            } catch (SQLException ex) {
            }
        }

        return listaClientes;
    }

    public static boolean salvarTurma(Turma p) {
        boolean retorno = false;
        Connection conexao = null;
        PreparedStatement instrucaoSQL = null;

        try {

            //Passo 1
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Passo 2 - DriverManager para abrir a conexão
            String URL = "jdbc:mysql://localhost:3306/ibcp?useTimezone=true&serverTimezone=UTC&useSSL=false";

            conexao = DriverManager.getConnection(URL, "root", "1234");
            conexao = GerenciadorConexao.abrirConexao();

            //Passo 3 - Executar uma instrução SQL
            instrucaoSQL = conexao.prepareStatement("call salvarturma(?,?,?,?,?,?)");

            //Adiciono os parâmetros ao meu comando SQL
            instrucaoSQL.setString(1, p.getCurso_id());
            instrucaoSQL.setString(2, p.getDescricao());
            instrucaoSQL.setDate(3, new java.sql.Date(p.getDt_inicio().getTime()));
            instrucaoSQL.setDate(4, new java.sql.Date(p.getDt_fim().getTime()));
            instrucaoSQL.setString(5, p.getTurma());
            instrucaoSQL.setString(6, p.getValor());

            //Executar a instrução SQL
            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                retorno = true;

            }

        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
            retorno = false;
        } finally {

            //Libero os recursos da memória
            try {
                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                }

                GerenciadorConexao.fecharConexao();
                conexao.close();

            } catch (SQLException ex) {
            }
        }

        return retorno;
    }

    public static ArrayList<Turma> todasturmas() {
        ResultSet rs = null;
        Connection conexao = null;
        PreparedStatement instrucaoSQL = null;

        //Armazeno as informaçoes da tabela (resultSet) em um ArrayList
        ArrayList<Turma> ListaTurmas = new ArrayList<Turma>();

        try {

            conexao = GerenciadorConexao.abrirConexao();
            //Passo 1
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Passo 2 - DriverManager para abrir a conexão
            String URL = "jdbc:mysql://localhost:3306/ibcp?useTimezone=true&serverTimezone=UTC&useSSL=false";

            conexao = DriverManager.getConnection(URL, "root", "1234");

            //Passo 3 - Executo a instrução SQL
            instrucaoSQL = conexao.prepareStatement("select concat('Turma ',nome) as 'Turma'\n"
                    + "from turma;");

            //Executa a Query (Consulta) - Retorna um objeto da classe ResultSet
            rs = instrucaoSQL.executeQuery();

            //Percorrer o resultSet
            while (rs.next()) {
                Turma p = new Turma();
                p.setTurma(rs.getString("Turma"));
                //Adiciono na listaClientes
                ListaTurmas.add(p);
            }

        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
            ListaTurmas = null;
        } finally {
            //Libero os recursos da memória

            try {
                if (rs != null) {
                    rs.close();
                }
                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                }

                conexao.close();
                GerenciadorConexao.fecharConexao();

            } catch (SQLException ex) {
            }
        }

        return ListaTurmas;
    }

    public static ArrayList<pagamento> consultarTurma(char pNome) {
        ResultSet rs = null;
        Connection conexao = null;
        PreparedStatement instrucaoSQL = null;

        ArrayList<pagamento> listaClientes = new ArrayList<pagamento>();

        try {

            conexao = GerenciadorConexao.abrirConexao();
            instrucaoSQL = conexao.prepareStatement("select a.id,m.matricula, a.nome, case when m.ativo = 0 then \"Não\"\n"
                    + "else 'Sim' end\n"
                    + "as 'Ativo',m.observacao,m.dt_matricula\n"
                    + "from aluno a\n"
                    + "inner join matricula m\n"
                    + "on m.alunos_id = a.id\n"
                    + "join turma t\n"
                    + "on t.id = m.turma_id\n"
                    + "where t.nome like ?"
                    + "order by a.nome;");

            //Adiciono os parâmetros ao meu comando SQL
            instrucaoSQL.setString(1, "%" + pNome + '%');

            rs = instrucaoSQL.executeQuery();

            while (rs.next()) {
                pagamento c = new pagamento();
                c.setId(rs.getInt("ID"));
                c.setMatricula(rs.getString("matricula"));
                c.setAluno(rs.getString("nome"));
                c.setAtivo(rs.getString("Ativo"));
                c.setObservacao(rs.getString("observacao"));
                c.setDatapag(rs.getDate("dt_matricula"));

                listaClientes.add(c);
            }

        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
            listaClientes = null;
        } finally {
            //Libero os recursos da memória
            try {
                if (rs != null) {
                    rs.close();
                }
                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                }

                GerenciadorConexao.fecharConexao();

            } catch (SQLException ex) {
            }
        }

        return listaClientes;
    }
    
    public static boolean salvarPsicanalista(psicanalista p) {
        boolean retorno = false;
        Connection conexao = null;
        PreparedStatement instrucaoSQL = null;

        try {

            //Passo 1
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Passo 2 - DriverManager para abrir a conexão
            String URL = "jdbc:mysql://localhost:3306/ibcp?useTimezone=true&serverTimezone=UTC&useSSL=false";

            conexao = DriverManager.getConnection(URL, "root", "1234");
            conexao = GerenciadorConexao.abrirConexao();

            //Passo 3 - Executar uma instrução SQL
            instrucaoSQL = conexao.prepareStatement("call inserirPsicanalista(?,?,?,?);");

            //Adiciono os parâmetros ao meu comando SQL
            instrucaoSQL.setString(1, p.getAtivo());
            instrucaoSQL.setString(2, p.getDidata());
            instrucaoSQL.setString(3, p.getNome());
            instrucaoSQL.setString(4, p.getobservacao());
           

            //Executar a instrução SQL
            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                retorno = true;

            }

        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
            retorno = false;
        } finally {

            //Libero os recursos da memória
            try {
                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                }

                GerenciadorConexao.fecharConexao();
                conexao.close();

            } catch (SQLException ex) {
            }
        }

        return retorno;
    }
}
