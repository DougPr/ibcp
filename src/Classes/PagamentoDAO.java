/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author dougl
 */
public class PagamentoDAO {

    public static ArrayList<pagamento> todosPagamentos() {
        ResultSet rs = null;
        Connection conexao = null;
        PreparedStatement instrucaoSQL = null;

        //Armazeno as informaçoes da tabela (resultSet) em um ArrayList
        ArrayList<pagamento> listaPagamentos = new ArrayList<pagamento>();

        try {

            conexao = GerenciadorConexao.abrirConexao();
            //Passo 1
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Passo 2 - DriverManager para abrir a conexão
            String URL = "jdbc:mysql://localhost:3306/ibcp?useTimezone=true&serverTimezone=UTC&useSSL=false";

            conexao = DriverManager.getConnection(URL, "root", "1234");

            //Passo 3 - Executo a instrução SQL
            instrucaoSQL = conexao.prepareStatement("select p.id, m.matricula, a.nome, p.dt_pagamento,Concat(p.mes_ref,'/',p.ano_ref) as mes_ref,p.vl_pago, f.nome as nomepg\n"
                    + "                    from pagamento p\n"
                    + "                    inner join forma_pagamento f\n"
                    + "                    on f.id = p.forma_pagamento_id\n"
                    + "                     join matricula m\n"
                    + "                     on p.matricula_id = m.id\n"
                    + "                     join aluno a\n"
                    + "                     on m.alunos_id = a.id");

            //Executa a Query (Consulta) - Retorna um objeto da classe ResultSet
            rs = instrucaoSQL.executeQuery();

            //Percorrer o resultSet
            while (rs.next()) {
                pagamento p = new pagamento();
                p.setId(rs.getInt("ID"));
                p.setMatricula(rs.getString("Matricula"));
                p.setAluno(rs.getString("nome"));
                p.setDatapag(rs.getDate("dt_pagamento"));
                p.setReferencia(rs.getString("mes_ref"));
                p.setValorpago(rs.getDouble("vl_pago"));
                p.setFormapagamento(rs.getString("nomepg"));

                //Adiciono na listaClientes
                listaPagamentos.add(p);
            }

        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
            listaPagamentos = null;
        } finally {
            //Libero os recursos da memória
            try {
                if (rs != null) {
                    rs.close();
                }
                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                }

                conexao.close();
                GerenciadorConexao.fecharConexao();

            } catch (SQLException ex) {
            }
        }

        return listaPagamentos;
    }

    public static ArrayList<pagamento> consultarPagamentos(String pdados) {
        ResultSet rs = null;
        Connection conexao = null;
        PreparedStatement instrucaoSQL = null;

        ArrayList<pagamento> listaPagamentos = new ArrayList<pagamento>();

        try {

            conexao = GerenciadorConexao.abrirConexao();
            instrucaoSQL = conexao.prepareStatement("select p.id,m.matricula, a.nome, p.dt_pagamento,Concat(p.mes_ref,'/',p.ano_ref) as mes_ref,p.vl_pago, f.nome as nomepg\n"
                    + "                    from pagamento p\n"
                    + "                    inner join forma_pagamento f\n"
                    + "                    on f.id = p.forma_pagamento_id\n"
                    + "                     join matricula m\n"
                    + "                     on p.matricula_id = m.id\n"
                    + "                     join aluno a\n"
                    + "                               on m.alunos_id = a.id"
                    + "                     where m.matricula like ? or a.nome like ?");

            //Adiciono os parâmetros ao meu comando SQL
            instrucaoSQL.setString(1, "%" + pdados + '%');
            instrucaoSQL.setString(2, "%" + pdados + '%');

            rs = instrucaoSQL.executeQuery();

            while (rs.next()) {
                pagamento c = new pagamento();
                c.setId(rs.getInt("ID"));
                c.setMatricula(rs.getString("Matricula"));
                c.setAluno(rs.getString("nome"));
                c.setDatapag(rs.getDate("dt_pagamento"));
                c.setReferencia(rs.getString("mes_ref"));
                c.setValorpago(rs.getDouble("vl_pago"));
                c.setFormapagamento(rs.getString("nomepg"));

                listaPagamentos.add(c);
            }

        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
            listaPagamentos = null;
        } finally {
            //Libero os recursos da memória
            try {
                if (rs != null) {
                    rs.close();
                }
                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                }

                GerenciadorConexao.fecharConexao();

            } catch (SQLException ex) {
            }
        }

        return listaPagamentos;
    }

    public static boolean salvar(pagamento p) {
        boolean retorno = false;
        Connection conexao = null;
        PreparedStatement instrucaoSQL = null;

        try {

            //Passo 1
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Passo 2 - DriverManager para abrir a conexão
            String URL = "jdbc:mysql://localhost:3306/ibcp?useTimezone=true&serverTimezone=UTC&useSSL=false";

            conexao = DriverManager.getConnection(URL, "root", "1234");
            conexao = GerenciadorConexao.abrirConexao();

            //Passo 3 - Executar uma instrução SQL
            instrucaoSQL = conexao.prepareStatement("call fazerPagamento(?,?,?,?,?,?,?,?,?);");

            //Adiciono os parâmetros ao meu comando SQL
            instrucaoSQL.setString(1, p.getAnoRef());
            instrucaoSQL.setDate(2, new java.sql.Date(p.getDatapag().getTime()));
            instrucaoSQL.setString(3, p.getFormapagamento());
            instrucaoSQL.setDouble(4, p.getJuros());
            instrucaoSQL.setString(5, p.getMatricula());
            instrucaoSQL.setString(6, p.getMesRef());
            instrucaoSQL.setString(7, p.getObservacao());
            instrucaoSQL.setDouble(8, p.getMulta());
            instrucaoSQL.setDouble(9, p.getValorpago());

            //Executar a instrução SQL
            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                retorno = true;

            }

        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
            retorno = false;
        } finally {

            //Libero os recursos da memória
            try {
                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                }

                GerenciadorConexao.fecharConexao();
                conexao.close();

            } catch (SQLException ex) {
            }
        }

        return retorno;
    }

    public static boolean excluir(String pNome, Date datinha) {
        boolean retorno = false;
        Connection conexao = null;
        PreparedStatement instrucaoSQL = null;

        try {

            //Tenta estabeler a conexão com o SGBD e cria comando a ser executado conexão
            //Obs: A classe GerenciadorConexao já carrega o Driver e define os parâmetros de conexão
            //Passo 1
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Passo 2 - DriverManager para abrir a conexão
            String URL = "jdbc:mysql://localhost:3306/ibcp?useTimezone=true&serverTimezone=UTC&useSSL=false";

            conexao = DriverManager.getConnection(URL, "root", "1234");

            instrucaoSQL = conexao.prepareStatement("DELETE FROM pagamento WHERE dt_pagamento like ? and matricula_id = (select id from matricula where matricula like ?)");

            //Adiciono os parâmetros ao meu comando SQL
            instrucaoSQL.setString(2, pNome);
            instrucaoSQL.setDate(1, new java.sql.Date(datinha.getTime()));

            //Mando executar a instrução SQL
            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                retorno = true;
            } else {
                retorno = false;
            }

        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
            retorno = false;
        } finally {

            //Libero os recursos da memória
            try {
                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                }

                GerenciadorConexao.fecharConexao();
                conexao.close();

            } catch (SQLException ex) {
            }
        }

        return retorno;
    }

    public static boolean alterar(pagamento p) {
        boolean retorno = false;
        Connection conexao = null;
        PreparedStatement instrucaoSQL = null;

        try {

            //Tenta estabeler a conexão com o SGBD e cria comando a ser executado conexão
            //Obs: A classe GerenciadorConexao já carrega o Driver e define os parâmetros de conexão
            conexao = GerenciadorConexao.abrirConexao();

            //Passo 1
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Passo 2 - DriverManager para abrir a conexão
            String URL = "jdbc:mysql://localhost:3306/ibcp?useTimezone=true&serverTimezone=UTC&useSSL=false";

            conexao = DriverManager.getConnection(URL, "root", "1234");

            instrucaoSQL = conexao.prepareStatement("call alterarPagamento(?,?,?,?,?,?,?,?,?,?)");

            //Adiciono os parâmetros ao meu comando SQL
            instrucaoSQL.setString(1, p.getAnoRef());
            instrucaoSQL.setDate(2, new java.sql.Date(p.getDatapag().getTime()));
            instrucaoSQL.setString(3, p.getFormapagamento());
            instrucaoSQL.setDouble(4, p.getJuros());
            instrucaoSQL.setString(5, p.getMatricula());
            instrucaoSQL.setString(6, p.getMesRef());
            instrucaoSQL.setString(7, p.getObservacao());
            instrucaoSQL.setDouble(8, p.getMulta());
            instrucaoSQL.setDouble(9, p.getValorpago());
            instrucaoSQL.setInt(10, p.getId());

            //Mando executar a instrução SQL
            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                retorno = true;
            } else {
                retorno = false;
            }

        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
            retorno = false;
        } finally {

            //Libero os recursos da memória
            try {
                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                }

                GerenciadorConexao.fecharConexao();
                conexao.close();

            } catch (SQLException ex) {
            }
        }

        return retorno;
    }

    public static ArrayList<pagamento> todosVencimentosMes() {
        ResultSet rs = null;
        Connection conexao = null;
        PreparedStatement instrucaoSQL = null;

        //Armazeno as informaçoes da tabela (resultSet) em um ArrayList
        ArrayList<pagamento> listaPagamentos = new ArrayList<pagamento>();

        try {

            conexao = GerenciadorConexao.abrirConexao();
            //Passo 1
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Passo 2 - DriverManager para abrir a conexão
            String URL = "jdbc:mysql://localhost:3306/ibcp?useTimezone=true&serverTimezone=UTC&useSSL=false";

            conexao = DriverManager.getConnection(URL, "root", "1234");

            //Passo 3 - Executo a instrução SQL
            instrucaoSQL = conexao.prepareStatement("select distinct a.id, a.nome, m.matricula, case when m.ativo = 1 then 'Sim' else 'Não' end as ativo \n"
                    + "from aluno a \n"
                    + "inner join matricula m\n"
                    + "on a.id = m.alunos_id\n"
                    + "inner join pagamento p\n"
                    + "on p.matricula_id = m.id \n"
                    + "inner join turma t\n"
                    + "on t.id = m.turma_id\n"
                    + "where p.ano_ref <> YEAR (CURDATE()) and p.mes_ref <> month(curdate()) "
                    + "and t.dt_fim > (select curdate()) and t.dt_inicio < (select curdate())  and m.ativo = 1\n"
                    + "order by a.nome;");

            //Executa a Query (Consulta) - Retorna um objeto da classe ResultSet
            rs = instrucaoSQL.executeQuery();

            //Percorrer o resultSet
            while (rs.next()) {
                pagamento p = new pagamento();
                p.setId(rs.getInt("ID"));
                p.setAluno(rs.getString("nome"));
                p.setMatricula(rs.getString("matricula"));
                p.setAtivo(rs.getString("ativo"));

                //Adiciono na listaClientes
                listaPagamentos.add(p);
            }

        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
            listaPagamentos = null;
        } finally {
            //Libero os recursos da memória
            try {
                if (rs != null) {
                    rs.close();
                }
                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                }

                conexao.close();
                GerenciadorConexao.fecharConexao();

            } catch (SQLException ex) {
            }
        }

        return listaPagamentos;
    }

    public static ArrayList<pagamento> consultarVencmNome(String pNome) {
        ResultSet rs = null;
        Connection conexao = null;
        PreparedStatement instrucaoSQL = null;

        ArrayList<pagamento> listaClientes = new ArrayList<pagamento>();

        try {

            conexao = GerenciadorConexao.abrirConexao();
            instrucaoSQL = conexao.prepareStatement("select distinct a.id, a.nome, m.matricula, case when m.ativo = 1 then 'Sim' else 'Não' end as ativo \n"
                    + "from aluno a \n"
                    + "inner join matricula m\n"
                    + "on a.id = m.alunos_id\n"
                    + "inner join pagamento p\n"
                    + "on p.matricula_id = m.id \n"
                    + "inner join turma t\n"
                    + "on t.id = m.turma_id\n"
                    + "where p.ano_ref <> YEAR (CURDATE()) and p.mes_ref <> month(curdate()) "
                    + "and t.dt_fim > (select curdate()) and t.dt_inicio < (select curdate())  \n"
                    + "and m.ativo = 1 and a.nome like ? or m.matricula like ?\n"
                    + "order by a.nome;");

            //Adiciono os parâmetros ao meu comando SQL
            instrucaoSQL.setString(1, "%" + pNome + '%');
            instrucaoSQL.setString(2, "%" + pNome + '%');

            rs = instrucaoSQL.executeQuery();

            while (rs.next()) {
                pagamento c = new pagamento();
                c.setId(rs.getInt("ID"));
                c.setAluno(rs.getString("nome"));
                c.setMatricula(rs.getString("matricula"));
                c.setAtivo(rs.getString("ativo"));

                listaClientes.add(c);
            }

        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
            listaClientes = null;
        } finally {
            //Libero os recursos da memória
            try {
                if (rs != null) {
                    rs.close();
                }
                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                }

                GerenciadorConexao.fecharConexao();

            } catch (SQLException ex) {
            }
        }

        return listaClientes;
    }

}
