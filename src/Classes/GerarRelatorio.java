/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;


import static java.awt.Frame.MAXIMIZED_BOTH;
import java.awt.Image;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JRViewer;
import net.sf.jasperreports.view.JasperViewer;



/**
 *
 * @author dougl
 */
public class GerarRelatorio {

    public String relan;

    public String getRelan() {
        return relan = "1";
    }

    public void setRelan(String relan) {
        this.relan = relan;
    }

    public void gerar() throws JRException, SQLException {

        try {

            String arquivoJasper = "C:\\Projetos\\ibcp\\SystemBook\\Rel_Cliente_Geral.jasper";
            HashMap parametros = new HashMap();
            Connection conn = GerenciadorConexao.abrirConexao();//Tenho uma classe conexão

            //Passando o parâmetro para o relatório
            parametros.put("rdi", Integer.parseInt(getRelan()));//Passando o valor de um JtextField para o relatório como parâmetro

            //Executa o relatório
            JasperPrint impressao = null;
            impressao = JasperFillManager.fillReport(arquivoJasper, parametros, conn);

            //Exibe o relatório
            JasperViewer viewer = new JasperViewer(impressao, false);
            
            //viewer.setTitle("RDI - Relatório de Divergência de Inventário");
            //viewer.setExtendedState(Frame.MAXIMIZED_BOTH);
            //viewer.setVisible(true);
            //TelaRelJasper trj = new TelaRelJasper();
            
            JFrame frame = new JFrame("Relátorio Cliente");
            frame.setExtendedState(MAXIMIZED_BOTH);
            frame.getContentPane().add(new JRViewer(impressao));
            ImageIcon icone = new ImageIcon(getClass().getResource("/resources/System-computer-icon.png"));
            Image image = icone.getImage();
            frame.setIconImage(image);
            frame.pack();
            
            frame.setVisible(true);
            
            
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(GerarRelatorio.class.getName()).log(Level.SEVERE, null, ex);

        } catch (SQLException ex) {
            Logger.getLogger(GerarRelatorio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
