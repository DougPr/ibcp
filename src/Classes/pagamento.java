/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.util.Date;

/**
 *
 * @author dougl
 */
public class pagamento {
    private int id;
    private String Matricula;
    private String aluno;
    private Date datapag;
    private String referencia;
    private double valorpago;
    private String formapagamento;
    private String ativo;
    private String anoRef;
    private String mesRef;
    private double multa;
    private double juros;
    private String observacao;
    private String turmaaln;

    public String getTurmaaln() {
        return turmaaln;
    }

    public void setTurmaaln(String turmaaln) {
        this.turmaaln = turmaaln;
    }
    
    public pagamento() {
    }
    
    public pagamento(int pid,String pMatricula, String paluno, Date pdatapag,String preferencia, double pvalorpago, String pformapagamento, String pativo) {
        this.id = pid;
        this.Matricula = pMatricula;
        this.aluno = paluno;
        this.datapag = pdatapag;
        this.referencia = preferencia;
        this.valorpago = pvalorpago;
        this.formapagamento = pformapagamento;
        this.ativo = pativo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    
    
    public double getMulta() {
        return multa;
    }

    public void setMulta(double multa) {
        this.multa = multa;
    }

    public double getJuros() {
        return juros;
    }

    public void setJuros(double juros) {
        this.juros = juros;
    }

    
    
    public String getAnoRef() {
        return anoRef;
    }

    public void setAnoRef(String anoRef) {
        this.anoRef = anoRef;
    }

    public String getMesRef() {
        return mesRef;
    }

    public void setMesRef(String mesRef) {
        this.mesRef = mesRef;
    }

    
    
    public String getAtivo() {
        return ativo;
    }

    public void setAtivo(String ativo) {
        this.ativo = ativo;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
    public String getMatricula() {
        return Matricula;
    }

    public void setMatricula(String Matricula) {
        this.Matricula = Matricula;
    }

    public String getAluno() {
        return aluno;
    }

    public void setAluno(String aluno) {
        this.aluno = aluno;
    }

    public Date getDatapag() {
        return datapag;
    }

    public void setDatapag(Date datapag) {
        this.datapag = datapag;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public double getValorpago() {
        return valorpago;
    }

    public void setValorpago(double valorpago) {
        this.valorpago = valorpago;
    }

    public String getFormapagamento() {
        return formapagamento;
    }

    public void setFormapagamento(String formapagamento) {
        this.formapagamento = formapagamento;
    }    
    
}
