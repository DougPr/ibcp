/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author dougl
 */
public class PendenciasDAO {
    
    public static ArrayList<Pendencias> todasPendencias() {
        ResultSet rs = null;
        Connection conexao = null;
        PreparedStatement instrucaoSQL = null;

        //Armazeno as informaçoes da tabela (resultSet) em um ArrayList
        ArrayList<Pendencias> ListaPendencias = new ArrayList<Pendencias>();

        try {

            conexao = GerenciadorConexao.abrirConexao();
            //Passo 1
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Passo 2 - DriverManager para abrir a conexão
            String URL = "jdbc:mysql://localhost:3306/ibcp?useTimezone=true&serverTimezone=UTC&useSSL=false";

            conexao = DriverManager.getConnection(URL, "root", "1234");

            //Passo 3 - Executo a instrução SQL
            instrucaoSQL = conexao.prepareStatement("select id, case when cumprida = 0 then 'Não' else 'Sim' end as 'cumprida', date_created,\r\n" + 
            		"descricao\r\n" + 
            		",inclusao\r\n" + 
            		",last_updated\r\n" + 
            		",titulo,\r\n" + 
            		"execucao\r\n" + 
            		"from pendencia;");

            //Executa a Query (Consulta) - Retorna um objeto da classe ResultSet
            rs = instrucaoSQL.executeQuery();

            //Percorrer o resultSet
            while (rs.next()) {
                Pendencias p = new Pendencias();
                p.setId(rs.getInt("ID"));
                p.setCumprida(rs.getString("cumprida"));
                p.setCriacao(rs.getDate("date_created"));
                p.setDescrição(rs.getString("descricao"));
                p.setLastUpdate(rs.getDate("last_updated"));
                p.setTitulo(rs.getString("titulo"));
                p.setExecucao(rs.getString("execucao"));

                //Adiciono na listaClientes
                ListaPendencias.add(p);
            }

        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
            ListaPendencias = null;
        } finally {
            //Libero os recursos da memória
            try {
                if (rs != null) {
                    rs.close();
                }
                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                }

                conexao.close();
                GerenciadorConexao.fecharConexao();

            } catch (SQLException ex) {
            }
        }

        return ListaPendencias;
    }


 public static boolean excluir(int pId) {
        boolean retorno = false;
        Connection conexao = null;
        PreparedStatement instrucaoSQL = null;

        try {

            //Tenta estabeler a conexão com o SGBD e cria comando a ser executado conexão
            //Obs: A classe GerenciadorConexao já carrega o Driver e define os parâmetros de conexão
            //Passo 1
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Passo 2 - DriverManager para abrir a conexão
            String URL = "jdbc:mysql://localhost:3306/ibcp?useTimezone=true&serverTimezone=UTC&useSSL=false";

            conexao = DriverManager.getConnection(URL, "root", "1234");

            instrucaoSQL = conexao.prepareStatement("DELETE FROM pendencia WHERE id = ?");

            //Adiciono os parâmetros ao meu comando SQL
            instrucaoSQL.setInt(1, pId);
            

            //Mando executar a instrução SQL
            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                retorno = true;
            } else {
                retorno = false;
            }

        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
            retorno = false;
        } finally {

            //Libero os recursos da memória
            try {
                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                }

                GerenciadorConexao.fecharConexao();
                conexao.close();

            } catch (SQLException ex) {
            }
        }

        return retorno;
    }
}