/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.util.Date;

/**
 *
 * @author dougl
 */
public class matricula {

    private int id;
    private String nome;
    private char turma;
    private String matricula;
    private String ativo;
    private String observacao;
    private Date datamatri;
    private int diavenc;

    public int getDiavenc() {
        return diavenc;
    }

    public void setDiavenc(int diavenc) {
        this.diavenc = diavenc;
    }

    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDatamatri() {
        return datamatri;
    }

    public void setDatamatri(Date datamatri) {
        this.datamatri = datamatri;
    }

    public String getAtivo() {
        return ativo;
    }

    public void setAtivo(String ativo) {
        this.ativo = ativo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    
    
    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getNome() {
        return nome;
    }

    public char getTurma() {
        return turma;
    }

    public void setTurma(char turma) {
        this.turma = turma;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
