package Classes;

import java.util.Date;

/**
 *
 * @author dougl
 */
public class Cliente {

    private int id;
    private String nome;
    private String sexo;
    private String cpf;
    private String email;
    private String rg;
    private String Endereco;
    private String tel_celular;
    private String tel_comercial;
    private String tel_residencial;
    private String observacao;
    private String ativo;
    private Date dataini;
    private double valor;
    /**
     *
     */
    public Cliente() {
    }

    public Date getDataini() {
        return dataini;
    }

    public void setDataini(Date dataini) {
        this.dataini = dataini;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    /**
     *
     * @param pid
     * @param pNome
     * @param psexo
     * @param pobservacao
     * @param prg
     * @param ptel_celular
     * @param ptel_comercial
     * @param ptel_residencial
     * @param pdataNascimento
     * @param pestadocivil
     * @param pCPF
     * @param pemail
     * @param pEndereco
     */
    public Cliente(int pid,String pativo, String pNome, String psexo,String pobservacao, String prg, String ptel_celular,String ptel_comercial,String ptel_residencial, String pdataNascimento,
            String pestadocivil, String pCPF, String pemail, String pEndereco) {
        this.id = pid;
        this.nome = pNome;
        this.ativo =pativo;
        this.sexo = psexo;
        this.tel_celular = ptel_celular;
        this.tel_comercial = ptel_comercial;
        this.tel_residencial = ptel_residencial;
        this.email = pemail;
        this.cpf = pCPF;
        this.Endereco = pEndereco;
        this.rg = prg;
        this.observacao = pobservacao;
    }

    public String getAtivo() {
        return ativo;
    }

    public void setAtivo(String ativo) {
        this.ativo = ativo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getEndereco() {
        return Endereco;
    }

    public void setEndereco(String Endereco) {
        this.Endereco = Endereco;
    }

    public String getTel_celular() {
        return tel_celular;
    }

    public void setTel_celular(String tel_celular) {
        this.tel_celular = tel_celular;
    }

    public String getTel_comercial() {
        return tel_comercial;
    }

    public void setTel_comercial(String tel_comercial) {
        this.tel_comercial = tel_comercial;
    }

    public String getTel_residencial() {
        return tel_residencial;
    }

    public void setTel_residencial(String tel_residencial) {
        this.tel_residencial = tel_residencial;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    /**
     *
     * @return
     */
}
