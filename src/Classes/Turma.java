/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.util.Date;

/**
 *
 * @author dougl
 */
public class Turma {
    private int id;
    private String curso_id;
    private String descricao;
    private Date dt_fim;
    private Date dt_inicio;
    private String turma;
    private String valor;
    
    
    public Turma (int pid, String pcurso_id,String pdescricao, Date pdt_fim,Date pdt_inicio, String pturma, String pvalor){
        this.id = pid;
        this.curso_id = pcurso_id;
        this.descricao = pdescricao;
        this.dt_fim = pdt_fim;
        this.dt_inicio = pdt_inicio;
        this.turma = pturma;
        this.valor = pvalor;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
    
    public Turma() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCurso_id() {
        return curso_id;
    }

    public void setCurso_id(String curso_id) {
        this.curso_id = curso_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getDt_fim() {
        return dt_fim;
    }

    public void setDt_fim(Date dt_fim) {
        this.dt_fim = dt_fim;
    }

    public Date getDt_inicio() {
        return dt_inicio;
    }

    public void setDt_inicio(Date dt_inicio) {
        this.dt_inicio = dt_inicio;
    }

    public String getTurma() {
        return turma;
    }

    public void setTurma(String turma) {
        this.turma = turma;
    }
}
